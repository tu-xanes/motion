#!/usr/bin/python3
import epics
import numpy as np
import threading
import time

## event
eve = threading.Event()
def onchange(**kwargs):
  eve.set()

## connect to epics PVs
print(f"[{time.asctime()}] Connecting epics channels")
# xtal inputs
xtalp1x = epics.PV("XNS:MTN:dist:transf6.J", auto_monitor=True)
xtalp1y = epics.PV("XNS:MTN:dist:transf6.K", auto_monitor=True)
xtalp2x = epics.PV("XNS:MTN:dist:transf6.L", auto_monitor=True)
xtalp2y = epics.PV("XNS:MTN:dist:transf6.M", auto_monitor=True, callback=onchange)
# detector inputs
detp0x = epics.PV("XNS:MTN:dist:transf2.J", auto_monitor=True)
detp0y = epics.PV("XNS:MTN:dist:transf2.K", auto_monitor=True)
detp1x = epics.PV("XNS:MTN:dist:transf2.L", auto_monitor=True)
detp1y = epics.PV("XNS:MTN:dist:transf2.M", auto_monitor=True)
detp2x = epics.PV("XNS:MTN:dist:transf2.N", auto_monitor=True)
detp2y = epics.PV("XNS:MTN:dist:transf2.O", auto_monitor=True)
detp3x = epics.PV("XNS:MTN:dist:transf3.J", auto_monitor=True)
detp3y = epics.PV("XNS:MTN:dist:transf3.K", auto_monitor=True)
detp4x = epics.PV("XNS:MTN:dist:transf3.L", auto_monitor=True)
detp4y = epics.PV("XNS:MTN:dist:transf3.M", auto_monitor=True)
detp5x = epics.PV("XNS:MTN:dist:transf3.N", auto_monitor=True)
detp5y = epics.PV("XNS:MTN:dist:transf3.O", auto_monitor=True, callback=onchange)
# wall inputs
wallp1x = epics.PV("XNS:MTN:wall2:P1x", auto_monitor=True)
wallp1y = epics.PV("XNS:MTN:wall2:P1y", auto_monitor=True)
wallp2x = epics.PV("XNS:MTN:wall2:P2x", auto_monitor=True)
wallp2y = epics.PV("XNS:MTN:wall2:P2y", auto_monitor=True)
wallp3x = epics.PV("XNS:MTN:wall1:P1x", auto_monitor=True)
wallp3y = epics.PV("XNS:MTN:wall1:P1y", auto_monitor=True)
# path inputs
pathp2y = epics.PV("XNS:MTN:m1:transf1.D", auto_monitor=True, callback=onchange)
pathp3x = epics.PV("XNS:MTN:dist:transf1.G", auto_monitor=True)
pathp3y = epics.PV("XNS:MTN:dist:transf1.H", auto_monitor=True, callback=onchange)
# xtal outputs
outxtalx = epics.PV("XNS:MTN:dist:xtal-plane:x", auto_monitor=False)
outxtaly = epics.PV("XNS:MTN:dist:xtal-plane:y", auto_monitor=False)
# det outputs
outdetx = epics.PV("XNS:MTN:dist:det-edges:x", auto_monitor=False)
outdety = epics.PV("XNS:MTN:dist:det-edges:y", auto_monitor=False)
# wall outputs
outwallx = epics.PV("XNS:MTN:dist:wall1-corners:x", auto_monitor=False)
outwally = epics.PV("XNS:MTN:dist:wall1-corners:y", auto_monitor=False)
# path outputs
outpathx = epics.PV("XNS:MTN:dist:wall2-corners:x", auto_monitor=False)
outpathy = epics.PV("XNS:MTN:dist:wall2-corners:y", auto_monitor=False)
# end of pv channels
time.sleep(2)

## main loop
print(f"[{time.asctime()}] Listening...")
while(True):
  try:
    eve.wait()
    outxtalx.put([xtalp1x.value, xtalp2x.value])
    outxtaly.put([xtalp1y.value, xtalp2y.value])
    outdetx.put([detp0x.value, detp1x.value, detp2x.value, detp3x.value, detp4x.value, detp5x.value, detp0x.value])
    outdety.put([detp0y.value, detp1y.value, detp2y.value, detp3y.value, detp4y.value, detp5y.value, detp0y.value])
    outwallx.put([wallp1x.value, wallp2x.value, wallp3x.value])
    outwally.put([wallp1y.value, wallp2y.value, wallp3y.value])
    outpathx.put([0.0, 0.0, pathp3x.value])
    outpathy.put([0.0, pathp2y.value, pathp3y.value])
    eve.clear()
  except Exception as errr:
    print(f"[time.asctime()] Exception caught.")
    print(errr)
    time.sleep(1)
    eve.clear()


print("OK")
