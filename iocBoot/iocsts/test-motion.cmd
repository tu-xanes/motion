#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Macros test
## tests
epicsEnvSet("P", "XNS:MTN:")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("motion.db","P=$(P)")
dbLoadRecords("mconv.db","P=$(P),CH=m1")
dbLoadRecords("mconv.db","P=$(P),CH=m2")
dbLoadRecords("mconv.db","P=$(P),CH=m3")
dbLoadRecords("mconv.db","P=$(P),CH=m4")
dbLoadRecords("mconv.db","P=$(P),CH=m5")
dbLoadRecords("mconv.db","P=$(P),CH=m6")
dbLoadRecords("mconv.db","P=$(P),CH=m7")
dbLoadRecords("mconv.db","P=$(P),CH=m8")

## select records
dbLoadRecords("msel.db","P=$(P)xtal:")
dbLoadRecords("msel.db","P=$(P)filters:")
dbLoadRecords("msel.db","P=$(P)dist:")

## walls
dbLoadRecords("wall.db","P=$(P),R=wall1")
dbLoadRecords("wall.db","P=$(P),R=wall2")

## distance warning 
dbLoadRecords("distwarn.db","P=$(P)dist:")

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_requestfile_path("$(TOP)/iocBoot/$(IOC)")
set_requestfile_path("$(CALC)/calcApp/Db")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30, "P=$(P)")

## Start any sequence programs
#seq sncxxx,"user=epics"
